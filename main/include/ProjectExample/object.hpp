#pragma once

namespace uqac::world
{
	class Object
	{
	public:
		Object() = default;
		~Object() = default;
	};
}